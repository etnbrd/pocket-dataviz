import * as d3 from "d3";

function sortBy(attr) {
  return function(a, b) {
    return a[attr] - b[attr];
  }
}

function dateToTime(d) {
  return d.getHours() * 60 * 60 * 1000 + d.getMinutes() * 60 * 1000 + d.getSeconds() * 1000;
}

/* -------------------------------------------------------------------------- */

function fetchJson(filename) {

  return fetch(filename)
    .then(function(response) {
      return response.json();
    })
}

/* -------------------------------------------------------------------------- */

function prepare(data) {

  console.log("prepare");

  // return fetchJson('data/output.data')
    // .then(function(data) {

    console.log(data.list)

      var items = Object.keys(data.list)
        .map(function(id) {
          return data.list[id];
        })
        .map(function(item) {
          // Parse times into dates
          item.date_added = new Date(item.time_added * 1000);
          if (item.time_read > 0) {
            item.date_read = new Date(item.time_read * 1000);
            // Compute time waited from added to read.
            item.time_waiting = ((item.time_read === 0 ? (Date.now()/1000) : item.time_read) - item.time_added) / 604800;
          }


          return item;
        })
        .sort(sortBy('time_added'))

      var reads = items.filter(function(item) {
        return item.time_read > 0;
      })

      var pendings = items.filter(function(item) {
        return item.time_read === 0;
      })

      var sum = 0;
      var word_count = 0;
      var time = 0;

      // Building the list of every add and read event from the items.
      // This list allows to know, for each event:
      // - the instant sum of item
      // - the total word count
      var events = [].concat(
        // Gather all the items at the time they were added
        items.map(function(item) {
          return {
            item: item,
            date: item.date_added,
            reduce: 1,
            word_count: 0
          };
        }),
        // Gather all the items, if read, at the time they were read
        items.map(function(item) {
          return item.time_read > 0 ? {
            item: item,
            date: item.date_read,
            reduce: -1,
            word_count: item.word_count,
          } : undefined;
        })
        // Filter out the items that are not read yet
        .filter(function(d) {
          return d;
        })
      )
      .sort(sortBy('date'))
      // From all the added and read date for each item,
      // build the instant count of items in the list, at every date.
      .map(function(event) {
        sum += event.reduce;
        word_count += +event.word_count || 0;
        if (event.reduce > 0) { // The instant count in the list when adding the item
          event.item.sum_added = sum;
        }

        if (event.reduce < 0) { // The instant count in the list when reading the item
          event.item.sum_read = sum;
        }

        return {
          date: event.date,
          item: event.item,
          sum: sum,
          word_count: word_count
        };
      })

      // Add a starting point
      events.unshift({
        date: events[0].date,
        item: undefined,
        sum: 0,
        word_count: 0
      })

      // var timeReads = reads.map(function(item) {
      //   return {
      //     time: dateToTime(item.date_read),
      //   }
      // })

      var time_reads = d3.nest()
        .key(function(d) {
          var time = dateToTime(d.date_read);
          return Math.floor(time/2000000)*2000000;
        })
        .rollup(function(items) {
          return items.length;
        })
        .entries(reads)
        .map(function(d) {
          return {time: +d.key, sum: d.value};
        })
        .sort(sortBy('time'))

      var elapsed = Date.now() - events[0].date;
      var first_day = events[0].date;
      var average_word_count = word_count / reads.length;

      return {
        items,
        reads,
        pendings,
        events,
        time_reads,
        word_count,
        average_word_count,
        elapsed,
        first_day
      }
    // });
}

export default prepare;
