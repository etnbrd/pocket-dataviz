function isFunction(functionToCheck) {
 var getType = {};
 return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

function getterFactory(name) {
  return function(data) {
    return data[name];
  }
}

function functionFiller(fn) {
  return isFunction(fn) ? fn : getterFactory(fn);
}

export function dots(options) {

  options.data = functionFiller(options.data);
  options.cx = functionFiller(options.cx);
  options.cy = functionFiller(options.cy);
  options.r = functionFiller(options.r);

  return {
    init(chart, scales, data) {
      chart.selectAll('.' + options.class)
      .data(data.pendings)
      .enter()
      .append('circle')
      .attr('class', options.class)
      .attr('cx', d => scales.x(d.date_added))
      .attr('cy', scales.s(0))
      .attr('r', 2);
    },
    toWeeks(chart, scales, t) {
      chart.selectAll('.' + options.class)
      .transition(t)
      .attr('cy', d => scales.y(d.time_waiting))
      .attr('r', 1)
      .attr("fill", 'rgba(71, 190, 255, .5)')
    },
    toCount(chart, scales, t) {
      chart.selectAll('.' + options.class)
      .transition(t)
      .attr('cy', d => scales.s(d.sum_added))
      .attr('r', 1)
      .attr("fill", 'rgba(71, 190, 255, .01)');
    }
  }
}
