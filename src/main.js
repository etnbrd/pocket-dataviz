import Vue from 'vue'
import App from './App.vue'

var el = document.createElement('div')
document.body.append(el)

new Vue({
  el: el,
  render: h => h(App)
})
