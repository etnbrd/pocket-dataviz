import storage from 'local-storage-fallback';

const key = '70585-9118e1240c03628126091bd7';
const redirect_uri = "https://etnbrd.gitlab.io/pocket-dataviz/";
const cors_proxy = "https://cors-anywhere.herokuapp.com/";
const pocket_uri = "https://getpocket.com/v3/";

// function request(endpoint, body) {
//   return fetch(cors_proxy + pocket_uri + endpoint, {
//     'method': 'POST',
//     'headers': {
//       'Content-Type': 'application/json',
//       'X-Accept': 'application/json'
//     },
//     'body': body
//   })
//   .then(function(res){
//     return res.json();
//   })
// }

// function getRequest() {
//   const endpoint =  "/oauth/request";
//   const body = JSON.stringify({
//     'consumer_key': key,
//     'redirect_uri': redirect_uri,
//   });
//
//   return request(endpoint, body)
// }



function getRequest() {

  console.log(">>> getRequest");

  const cred = {
    'consumer_key': key,
    'redirect_uri': redirect_uri,
  }

  var body = JSON.stringify(cred);

  return fetch(cors_proxy + "https://getpocket.com/v3/oauth/request", {
    'method': 'POST',
    'headers': {
      'Content-Type': 'application/json',
      'X-Accept': 'application/json'
    },
    'body': body
  })
  .then(function(res){
    console.log(res);
    return res.json();
  })
  .then(function(data){

    console.log(">>> got request_token : ", data);
    storage.setItem('request_token', data.code);
    window.location.replace('https://getpocket.com/auth/authorize?request_token=' + data.code + '&redirect_uri=' + redirect_uri);
  })
}

function getAccess(request_token) {

  console.log(">>> getAccess", request_token);

  const cred = {
    'consumer_key': key,
    'code': request_token,
  }

  var body = JSON.stringify(cred);

  console.log(body);

  return fetch(cors_proxy + "https://getpocket.com/v3/oauth/authorize", {
    'method': 'POST',
    'headers': {
      'Content-Type': 'application/json',
      'X-Accept': 'application/json'
    },
    'body': body
  })
  .then(function(res){
    console.log(res);
    return res.json();
  })
  .then(function(data){
    console.log(">>> got access_token : ", data);
    storage.setItem('access_token', data.code);
    // return data.access_token;
    return Promise.resolve(data.access_token);
  })
}


function getData(access_token) {

  console.log(">>> get data", access_token);

  const cred = {
    'consumer_key': key,
    'access_token': access_token,
  }

  var body = JSON.stringify(cred);

  return fetch(cors_proxy + "https://getpocket.com/v3/get", {
    'method': 'POST',
    'headers': {
      'Content-Type': 'application/json',
      'X-Accept': 'application/json'
    },
    'body': body
  })
  .then(function(res){
    console.log(res);
    return res.json();
  })
}

export default {
  getRequest,
  getAccess,
  getData
}
